class ExamsController < ApplicationController

    #untuk menampilkan form data baru
    def new 
     @exam = Exam.new
   end
 
   # untuk memproses data baru yang di masukan di form new
   def create      
     exam = Exam.new(resources_params)
     exam.save
     flash[:notice] = "exam #{exam.title} Berhasil di Tambah."
     redirect_to exams_path
   end
 
   #menampilkan data yang sudah disimpan di edit
   def edit
     id = params[:id]
     @exam = Exam.find id  
   end
 
   #melakukan proses ketika user mengedit data
   def update
     id = params[:id]
     @exam = Exam.find(id)
     @exam.update(resources_params) 
     flash[:notice] = "exam #{@exam.title} Berhasil di edit."  
     redirect_to exam_path(@exam)
   end
 
   #menghapus data
   def destroy
     id = params[:id]
     @exam = Exam.find(id)
     @exam.destroy
     flash[:notice] = "Buku #{@exam.title} Berhasil di Hapus."
     redirect_to exams_path
   end
 
   #menampilkan seluruh data yang ada di database
   def index
     @exams = Exam.all    
   end
 
   #menampilkan sebuah data secara detail
   def show
     id = params[:id]
     @exam = Exam.find id  
   end
 
   private #hanya bisa dipanggil dalam satu class
   def resources_params
     params.require(:exam).permit(:title, :mapel, :duration, :status, :level, :student_id)
   end
end
