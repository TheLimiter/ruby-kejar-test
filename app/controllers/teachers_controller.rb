class TeachersController < ApplicationController
   #untuk menampilkan form data baru
   def new 
    @teacher = Teacher.new
  end

  # untuk memproses data baru yang di masukan di form new
  def create      
    teacher = Teacher.new(resources_params)
    teacher.save
    flash[:notice] = "Teacher #{teacher.name} Berhasil di Tambah."
    redirect_to teachers_path
  end

  #menampilkan data yang sudah disimpan di edit
  def edit
    id = params[:id]
    @teacher = Teacher.find id  
  end

  #melakukan proses ketika user mengedit data
  def update
    id = params[:id]
    @teacher = Teacher.find(id)
    @teacher.update(resources_params) 
    flash[:notice] = "Teacher #{@teacher.name} Berhasil di edit."  
    redirect_to teacher_path(@teacher)
  end

  #menghapus data
  def destroy
    id = params[:id]
    @teacher = Teacher.find(id)
    @teacher.destroy
    flash[:notice] = "Buku #{@teacher.name} Berhasil di Hapus."
    redirect_to teachers_path
  end

  #menampilkan seluruh data yang ada di database
  def index
    @teachers = Teacher.all    
  end

  #menampilkan sebuah data secara detail
  def show
    id = params[:id]
    @teacher = Teacher.find id  
  end

  private #hanya bisa dipanggil dalam satu class
  def resources_params
    params.require(:teacher).permit(:nik, :name, :age, :kelas, :mapel)
  end
end
