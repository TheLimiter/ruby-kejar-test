class StudentsController < ApplicationController
  #untuk menampilkan form data baru
  def new 
    @student = Student.new
  end

  # untuk memproses data baru yang di masukan di form new
  def create      
    student = Student.new(resources_params)
    student.save
    flash[:notice] = "Student #{student.name} Berhasil di Tambah."
    redirect_to students_path
  end

  #menampilkan data yang sudah disimpan di edit
  def edit
    id = params[:id]
    @student = Student.find id  
  end

  #melakukan proses ketika user mengedit data
  def update
    id = params[:id]
    @student = Student.find(id)
    @student.update(resources_params) 
    flash[:notice] = "Student #{@student.name} Berhasil di edit."  
    redirect_to student_path(@student)
  end

  #menghapus data
  def destroy
    id = params[:id]
    @student = Student.find(id)
    @student.destroy
    flash[:notice] = "Buku #{@student.name} Berhasil di Hapus."
    redirect_to students_path
  end

  #menampilkan seluruh data yang ada di database
  def index
    @students = Student.all    
  end

  #menampilkan sebuah data secara detail
  def show
    id = params[:id]
    @student = Student.find id  
  end

  private #hanya bisa dipanggil dalam satu class
  def resources_params
    params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :nik)
  end
end
