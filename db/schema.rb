# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_09_084633) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "exams", force: :cascade do |t|
    t.string "title", null: false
    t.string "mapel", null: false
    t.integer "duration"
    t.integer "nilai", default: 0, null: false
    t.string "status", default: "aktif"
    t.integer "level", default: 1, null: false
    t.integer "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id", "mapel", "level"], name: "index_exams_on_student_id_and_mapel_and_level"
  end

  create_table "payments", force: :cascade do |t|
    t.integer "id_transaction", null: false
    t.string "status", null: false
    t.text "upload", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id_transaction"], name: "index_payments_on_id_transaction"
  end

  create_table "pockets", force: :cascade do |t|
    t.integer "balance", default: 0, null: false
    t.integer "student_id", null: false
    t.integer "teacher_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id", "teacher_id"], name: "index_pockets_on_student_id_and_teacher_id"
  end

  create_table "reports", force: :cascade do |t|
    t.string "title", null: false
    t.string "hasil", null: false
    t.string "mapel", null: false
    t.integer "teacher_id", null: false
    t.integer "student_id", null: false
    t.date "timestamps"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mapel", "teacher_id", "student_id"], name: "index_reports_on_mapel_and_teacher_id_and_student_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "name", null: false
    t.string "username", null: false
    t.integer "age", null: false
    t.string "kelas", null: false
    t.text "address"
    t.string "city"
    t.string "nik", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["kelas", "nik"], name: "index_students_on_class_and_nik_and_class"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "nik", null: false
    t.string "name", null: false
    t.integer "age", null: false
    t.string "kelas", null: false
    t.string "mapel", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["kelas", "mapel"], name: "index_teachers_on_kelas_and_mapel"
  end

end
