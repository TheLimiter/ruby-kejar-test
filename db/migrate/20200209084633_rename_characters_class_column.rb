class RenameCharactersClassColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :students, :class, :kelas
    rename_column :teachers, :class, :kelas
  end
end
