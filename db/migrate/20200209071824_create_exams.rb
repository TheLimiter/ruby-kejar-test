class CreateExams < ActiveRecord::Migration[6.0]
  def change
    create_table :exams do |t|
      t.string :title, null: false
      t.string :mapel, null: false
      t.integer :duration
      t.integer :nilai, null: false, default: 0
      t.string :status, default: 'aktif'
      t.integer :level, default: 1, null: false
      t.integer :student_id, null: false, foreign_key: true

      t.timestamps
    end
    add_index :exams, [:student_id, :mapel, :level]
  end

  def down
    drop_table :exams
  end
end
