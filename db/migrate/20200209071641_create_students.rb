class CreateStudents < ActiveRecord::Migration[6.0]
  def up
    create_table :students do |t|
      t.string :name, null: false
      t.string :username, null: false
      t.integer :age, null: false
      t.string :class, null: false
      t.text :address
      t.string :city
      t.string :nik, null: false, unique_key: true

      t.timestamps      
    end

    add_index :students, [:class, :nik, :class]
  end

  def down
    drop_table :students
  end
end
