class CreatePockets < ActiveRecord::Migration[6.0]
  def change
    create_table :pockets do |t|
      t.integer :balance, null: false,default: 0
      t.integer :student_id, null: false, foreign_key: true
      t.integer :teacher_id, null: false, foreign_key: true

      t.timestamps
    end
    add_index :pockets, [:student_id, :teacher_id]
  end

  def down
    drop_table :pockets
  end
end
