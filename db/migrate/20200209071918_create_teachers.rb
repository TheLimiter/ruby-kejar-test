class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers do |t|
      t.string :nik, null: false, unique_key: true
      t.string :name, null: false
      t.integer :age, null: false
      t.string :class, null: false
      t.string :mapel, null: false

      t.timestamps
    end
    add_index :teachers, [:class, :mapel]
  end

  def down
    drop_table :teachers
  end
end
