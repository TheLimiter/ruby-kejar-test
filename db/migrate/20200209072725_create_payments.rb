class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.integer :id_transaction, null: false, foreign_key: true
      t.string :status, null: false
      t.text :upload, null: false

      t.timestamps
    end
    add_index :payments, [:id_transaction]
  end

  def down
    drop_table :payments
  end
end
