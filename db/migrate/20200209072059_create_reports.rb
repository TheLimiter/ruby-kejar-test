class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.string :title, null: false
      t.string :hasil, null: false
      t.string :mapel, null: false
      t.integer :teacher_id, null: false, foreign_key: true
      t.integer :student_id, null: false, foreign_key: true
      t.date :timestamps

      t.timestamps
    end
    add_index :reports, [:mapel, :teacher_id, :student_id]
  end

  def down
    drop_table :reports
  end
end
